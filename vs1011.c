/* Dooba SDK
 * VS1011 MP3 Decoder Driver
 */

// External Includes
#include <util/delay.h>
#include <spi/spi.h>
#include <dio/dio.h>

// Internal Includes
#include "vs1011.h"

// Initialize
void vs1011_init(struct vs1011 *v, uint8_t rst_pin, uint8_t dreq_pin, uint8_t cs_pin, uint8_t dcs_pin)
{
	// Pre-Init
	vs1011_preinit(v, rst_pin, dreq_pin, cs_pin, dcs_pin);

	// Setup Structure
	v->rst_pin = rst_pin;
	v->dreq_pin = dreq_pin;
	v->cs_pin = cs_pin;
	v->dcs_pin = dcs_pin;

	// Delay
	_delay_ms(VS1011_DELAY_INIT);

	// Initialize decoder
	dio_hi(rst_pin);

	// Delay
	_delay_ms(VS1011_DELAY_INIT);

	// Configure decoder
	vs1011_read_reg(v, VS1011_REG_MODE);
	vs1011_write_reg(v, VS1011_REG_MODE, VS1011_MODE_SDINEW);

	// Delay
	_delay_ms(VS1011_DELAY_INIT);

	// Reset Volume & Bass / Treble
	vs1011_set_vol(v, 0, 0);
	vs1011_set_bass_boost(v, 0, 0);
	vs1011_set_treble_boost(v, 0, 0);
}

// Pre-Initialize
void vs1011_preinit(struct vs1011 *v, uint8_t rst_pin, uint8_t dreq_pin, uint8_t cs_pin, uint8_t dcs_pin)
{
	// Configure Pins
	dio_output(rst_pin);
	dio_input(dreq_pin);
	dio_output(cs_pin);
	dio_output(dcs_pin);

	// Initialize Pins
	dio_lo(rst_pin);
	dio_hi(dreq_pin);
	dio_hi(cs_pin);
	dio_hi(dcs_pin);
}

// Set Volume
void vs1011_set_vol(struct vs1011 *v, uint8_t left, uint8_t right)
{
	uint16_t vol;

	// Check Max Volume
	if(left > VS1011_VOL_MAX)															{ left = VS1011_VOL_MAX; }
	if(right > VS1011_VOL_MAX)															{ right = VS1011_VOL_MAX; }

	// Prepare Volume
	left = VS1011_VOL_MAX - left;
	right = VS1011_VOL_MAX - right;
	vol = left;
	vol = vol * 256;
	vol = vol + right;

	// Set Volume
	vs1011_write_reg(v, VS1011_REG_VOL, vol);
}

// Set Bass Boost
void vs1011_set_bass_boost(struct vs1011 *v, uint8_t boost, uint8_t freqlimit_hz)
{
	uint16_t b;

	// Check Boost / Frequency Limit
	if(freqlimit_hz < VS1011_BASS_BOOST_FREQLIMIT_MIN)									{ freqlimit_hz = VS1011_BASS_BOOST_FREQLIMIT_MIN; }
	if(freqlimit_hz > VS1011_BASS_BOOST_FREQLIMIT_MAX)									{ freqlimit_hz = VS1011_BASS_BOOST_FREQLIMIT_MAX; }
	if(boost > VS1011_BASS_BOOST_MAX)													{ boost = VS1011_BASS_BOOST_MAX; }

	// Set Bass Boost
	b = vs1011_read_reg(v, VS1011_REG_BASS);
	b = (b & 0xff00) | ((boost << 4) | (freqlimit_hz / 10));
	vs1011_write_reg(v, VS1011_REG_BASS, b);
}

// Set Treble Boost
void vs1011_set_treble_boost(struct vs1011 *v, int8_t boost, uint8_t freqlimit_khz)
{
	uint16_t b;

	// Check Boost / Frequency Limit
	if(freqlimit_khz < VS1011_TREBLE_BOOST_FREQLIMIT_MIN)								{ freqlimit_khz = VS1011_TREBLE_BOOST_FREQLIMIT_MIN; }
	if(freqlimit_khz > VS1011_TREBLE_BOOST_FREQLIMIT_MAX)								{ freqlimit_khz = VS1011_TREBLE_BOOST_FREQLIMIT_MAX; }
	if(boost > VS1011_TREBLE_BOOST_MAX)													{ boost = VS1011_TREBLE_BOOST_MAX; }
	if(boost < VS1011_TREBLE_BOOST_MIN)													{ boost = VS1011_TREBLE_BOOST_MIN; }

	// Set Treble Boost
	b = vs1011_read_reg(v, VS1011_REG_BASS);
	b = (b & 0x00ff) | (((uint16_t)((boost << 4) | (freqlimit_khz / 10))) << 8);
	vs1011_write_reg(v, VS1011_REG_BASS, b);
}

// Send MP3 Data
uint16_t vs1011_send_data(struct vs1011 *v, uint8_t *d, uint16_t s)
{
	uint16_t i = 0;

	// Select SDI interface
	dio_lo(v->dcs_pin);

	// Transfer Data
	while((i < s) && (dio_rd(v->dreq_pin)))												{ SPDR = d[i]; while(!(SPSR & _BV(SPIF))) { /* NoOp */ } i = i + 1; }

	// De-Select SDI interface
	dio_hi(v->dcs_pin);

	return i;
}

// Wait for Busy Chip (DREQ)
uint8_t vs1011_wait_dreq(struct vs1011 *v)
{
	uint16_t i = 0;

	// Wait for a second
	while((i < VS1011_WAIT_STEPS) && (dio_rd(v->dreq_pin) == 0))						{ _delay_us(VS1011_WAIT_STEP); }
	if(i >= VS1011_WAIT_STEPS)															{ return 1; }

	return 0;
}

// Read Register
uint16_t vs1011_read_reg(struct vs1011 *v, uint8_t addr)
{
	uint16_t r;

	// Always wait until SCI is ready before issuing a command
	if(vs1011_wait_dreq(v))																{ return 0; }

	// Select SCI interface
	dio_lo(v->cs_pin);

	// Send Read Command
	spi_io(VS1011_CMD_READ);

	// Send Address
	spi_io(addr);

	// Receive Data from Register
	r = spi_io(0xff);
	r = r << 8;
	r = r | spi_io(0xff);

	// De-Select SCI interface
	dio_hi(v->cs_pin);

	return r;
}

// Write Register
void vs1011_write_reg(struct vs1011 *v, uint8_t addr, uint16_t val)
{
	// Always wait until SCI is ready before issuing a command
	if(vs1011_wait_dreq(v))																{ return; }

	// Select SCI interface
	dio_lo(v->cs_pin);

	// Send Write Command
	spi_io(VS1011_CMD_WRITE);

	// Send Address
	spi_io(addr);

	// Send Data for Register
	spi_io(val >> 8);
	spi_io(val & 0xff);

	// De-Select SCI interface
	dio_hi(v->cs_pin);
}
