/* Dooba SDK
 * VS1011 MP3 Decoder Driver
 */

#ifndef	__VS1011_H
#define	__VS1011_H

// External Includes
#include <stdint.h>
#include <avr/io.h>

// Commands
#define	VS1011_CMD_READ						0x03
#define	VS1011_CMD_WRITE					0x02

// Registers
#define	VS1011_REG_MODE						0x00
#define	VS1011_REG_STATUS					0x01
#define	VS1011_REG_BASS						0x02
#define	VS1011_REG_CLOCKF					0x03
#define	VS1011_REG_DECODE_TIME				0x04
#define	VS1011_REG_AUDATA					0x05
#define	VS1011_REG_WRAM						0x06
#define	VS1011_REG_WRAMADDR					0x07
#define	VS1011_REG_HDAT0					0x08
#define	VS1011_REG_HDAT1					0x09
#define	VS1011_REG_AIADDR					0x0a
#define	VS1011_REG_VOL						0x0b
#define	VS1011_REG_AICTRL0					0x0c
#define	VS1011_REG_AICTRL1					0x0d
#define	VS1011_REG_AICTRL2					0x0e
#define	VS1011_REG_AICTRL3					0x0f

// Mode Flags
#define	VS1011_MODE_DIFF					0x0001
#define	VS1011_MODE_LAYER12					0x0002
#define	VS1011_MODE_RESET					0x0004
#define	VS1011_MODE_OUTOFWAV				0x0008
#define	VS1011_MODE_ZERO1					0x0010
#define	VS1011_MODE_TESTS					0x0020
#define	VS1011_MODE_STREAM					0x0040
#define	VS1011_MODE_ZERO2					0x0080
#define	VS1011_MODE_DACT_FALL				0x0100
#define	VS1011_MODE_DORD_LSBF				0x0200
#define	VS1011_MODE_SDISHARE				0x0400
#define	VS1011_MODE_SDINEW					0x0800
#define	VS1011_MODE_ZERO3					0x1000
#define	VS1011_MODE_ZERO4					0x2000

// Maximum Volume
#define	VS1011_VOL_MAX						0xfe

// Min/Max Boost
#define	VS1011_BASS_BOOST_MAX				15
#define	VS1011_TREBLE_BOOST_MAX				7
#define	VS1011_TREBLE_BOOST_MIN				(-8)

// Min/Max Frequency Limits
#define	VS1011_BASS_BOOST_FREQLIMIT_MAX		150
#define	VS1011_BASS_BOOST_FREQLIMIT_MIN		20

#define	VS1011_TREBLE_BOOST_FREQLIMIT_MAX	15
#define	VS1011_TREBLE_BOOST_FREQLIMIT_MIN	1

// Delays
#define	VS1011_DELAY_INIT					500
#define	VS1011_DELAY_IO						10

// Wait Duration (us)
#define	VS1011_WAIT_DURATION				1000000
#define	VS1011_WAIT_STEP					100
#define	VS1011_WAIT_STEPS					(VS1011_WAIT_DURATION / VS1011_WAIT_STEP)

// VS1011 Structure
struct vs1011
{
	// I/O Configuration
	uint8_t rst_pin;
	uint8_t dreq_pin;
	uint8_t cs_pin;
	uint8_t dcs_pin;
};

// Initialize
extern void vs1011_init(struct vs1011 *v, uint8_t rst_pin, uint8_t dreq_pin, uint8_t cs_pin, uint8_t dcs_pin);

// Pre-Initialize
extern void vs1011_preinit(struct vs1011 *v, uint8_t rst_pin, uint8_t dreq_pin, uint8_t cs_pin, uint8_t dcs_pin);

// Set Volume
extern void vs1011_set_vol(struct vs1011 *v, uint8_t left, uint8_t right);

// Set Bass Boost
extern void vs1011_set_bass_boost(struct vs1011 *v, uint8_t boost, uint8_t freqlimit_hz);

// Set Treble Boost
extern void vs1011_set_treble_boost(struct vs1011 *v, int8_t boost, uint8_t freqlimit_khz);

// Send MP3 Data
extern uint16_t vs1011_send_data(struct vs1011 *v, uint8_t *d, uint16_t s);

// Wait for Busy Chip (DREQ)
extern uint8_t vs1011_wait_dreq(struct vs1011 *v);

// Read Register
extern uint16_t vs1011_read_reg(struct vs1011 *v, uint8_t addr);

// Write Register
extern void vs1011_write_reg(struct vs1011 *v, uint8_t addr, uint16_t val);

#endif
